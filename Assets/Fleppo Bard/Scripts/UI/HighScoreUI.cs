﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreUI : MonoBehaviour {
	Text HighScoreText;

	// Use this for initialization
	void Start () {
		HighScoreText = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		HighScoreText.text = "HighScore: " + GameControl.instance.HighScore;
	}
}
